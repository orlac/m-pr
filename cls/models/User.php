<?php
namespace cls\models;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author Antonio
 * orlac@rambler.ru
 */
require_once (dirname(__FILE__).'/IUTable.php');
class User extends IUTable {
    //put your code here
    const NOW = 'now()';
    public $id,
           $name,
           $email,
           $pass,
           $photo,
           $date_reg ;
    
    protected $fields = array('id', 'name', 'email', 'pass', 'photo', 'date_reg');
    
    public function __construct($id = null)
    {
        if($id)
            $this->id = $id;
    }


    public function insert()
    {
        $sql = "insert IUSER set ";
    }
    
    public function save()
    {
        $sql = "replace into user  ";
        $keys = array();
        $vals = array();
        foreach($this->fields as $val)
        {
            if(property_exists($this, $val) && $this->$val !== null)
            {       
                $keys[] = $val;
                switch($val)
                {
                    case 'pass':
                        $vals[] = "'".md5($this->$val)."'";
                        break;
                    case 'date_reg':
                        $vals[] = mysql_real_escape_string( $this->$val );
                        break;
                    default:
                        $vals[] = "'".mysql_real_escape_string($this->$val)."'";
                }
            }
        }
        $sql .= '('.implode(', ', $keys).') ';
        $sql .= ' values ';
        $sql .= ' ('.implode(', ', $vals).') ';
        if( mysql_query($sql) )
        {
            $this->id = (!$this->id)? mysql_insert_id () : $this->id;
            return true;
        }
        return false;
    }
    
    public function getOne()
    {
        return array_shift( $this->get(1) );
    }
    
    public function get($limit = null)
    {
        $sql = "select * from user ";
        $wheres = array();
        foreach($this->fields as $val)
        {
            if(property_exists($this, $val) && $this->$val !== null)
            {       
                $value = '';
                switch($val)
                {
                    case 'pass':
                        $value = "'".md5($this->$val)."'";
                        break;
                    case 'date_reg':
                        $value = mysql_real_escape_string($this->$val);
                        break;
                    default:
                        $value = "'".mysql_real_escape_string($this->$val)."'";
                }
                $wheres[] = $val." = ".$value."";
            }
        }
        $sql .= ' where '.implode(' AND ', $wheres );
        $sql .= ($limit !== null)? ' limit 1' : '';
        $result = array();
        if($r = mysql_query($sql))
            while($item = mysql_fetch_assoc($r) )
            {
                $u = new User();
                $u->load($item);
                $result[] = $u;
            }
        else
            return false;
        return $result;
    }
}

?>
