<?php
namespace cls\models;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IUTable
 *
 * @author Antonio
 * orlac@rambler.ru
 */
class IUTable {
    //put your code here
    protected $table = "";
    
    public function load($data)
    {
        if($data != null)
        {
            foreach($data as $key => $val)
            {
                if(property_exists($this, $key))
                    $this->$key = $val;
            }
            return;
        }
    }
    
    public function __call($name, $arguments) 
    {    
        if(method_exists($this, '_'.$name))
        {
            $m = '_'.$name;
            return $this->$m();
        }elseif(property_exists($this, $name))
        {
            return $this->$name;
        }
        return NULL;
    }
}

?>
