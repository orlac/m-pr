<?php
namespace cls\forms;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Registration
 *
 * @author Antonio
 * orlac@rambler.ru
 */
require_once (dirname(__FILE__).'/../html/TextInput.php');
require_once (dirname(__FILE__).'/../html/IElement.php');
require_once (dirname(__FILE__).'/../html/RePassword.php');
require_once (dirname(__FILE__).'/../html/Password.php');
require_once (dirname(__FILE__).'/../html/FileInput.php');
require_once (dirname(__FILE__).'/../html/USerForm.php');

class Registration extends \cls\html\USerForm {
    //put your code here
    public $file, $mail, $user_name;
    
    public function __construct($name = null, &$request = null) {
        parent::__construct($name, $request);
        $this->ini();
    }
    
    private function ini()
    {
        $this->user_name = new \cls\html\TextInput('user_name');
        $this->user_name->setLabel('Ваше имя');
        $this->user_name->notNUll();
        //$el->htmlAttr = " id='email' ";
        $this->addElement( $this->user_name );
        
        $this->mail = new \cls\html\TextInput('email');
        $this->mail->setLabel('e-mail');
        $this->mail->setMatch(\cls\html\IElement::EMAIL);
        //$el->htmlAttr = " id='email' ";
        $this->addElement( $this->mail );
        
        $el = new \cls\html\Password('password');
        //$el->htmlAttr = " id='password' ";
        $el->setLabel('пароль');
        $this->addElement( $el );

        $el2 = new \cls\html\RePassword('repassword');
        //$el2->htmlAttr = " id='repassword' ";
        $el2->setLabel('пароль еше раз');
        $el2->setPasswordField($el);
        $this->addElement( $el2 );
        
        $this->file = new \cls\html\FileInput('file');
        $this->file->path = I_PATCH;
        $this->file->type = \cls\html\FileInput::IMAGE;
        //$el2->htmlAttr = " id='repassword' ";
        $this->addElement( $this->file );
    }
}

?>
