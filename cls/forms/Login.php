<?php
namespace cls\forms;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author Antonio
 * orlac@rambler.ru
 */
require_once (dirname(__FILE__).'/../html/TextInput.php');
require_once (dirname(__FILE__).'/../html/IElement.php');
require_once (dirname(__FILE__).'/../html/RePassword.php');
require_once (dirname(__FILE__).'/../html/Password.php');
require_once (dirname(__FILE__).'/../html/FileInput.php');
require_once (dirname(__FILE__).'/../html/USerForm.php');
class Login extends \cls\html\USerForm {
    //put your code here
    public $password, $mail;
    
    public function __construct($name = null, &$request = null) {
        parent::__construct($name, $request);
        $this->ini();
    }
    
    private function ini()
    {
        $this->mail = new \cls\html\TextInput('email');
        $this->mail->setLabel('e-mail');
        $this->mail->notNUll();
        $this->addElement( $this->mail );
        
        $this->password = new \cls\html\Password('password');
        //$el->htmlAttr = " id='password' ";
        $this->password->setLabel('пароль');
        $this->password->notNUll();
        $this->password->setMatch(null);
        $this->addElement( $this->password );
    }
}

?>
