<?php
namespace cls;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PageNav
 *
 * @author Antonio
 * orlac@rambler.ru
 */
class PageNav {
    //put your code here
    const main = '',
          registration = 'reg-page',
          profile = 'profile',
          login = 'login',
          logout = 'logout';//'enter'  ;
    
    public static function goToPage($page, $args = array())
    {
        $str = http_build_query($args);
        if(!$page)
            header('Location: /' );
        header('Location: /index.php?page='.$page.( ( !empty($str) )? '&'.$str : '' ) );
        exit();
    }
    
    public static function getLink($page)
    {
        if(!$page)
            return '/';
        return '/index.php?page='.$page;
    }
    
    public static function isAuth()
    {
        if(!isset($_SESSION['id']) || $_SESSION['id'] === 0)
            return false;
        return (int)$_SESSION['id'];
    }
    
    public static function auth($id)
    {
        $_SESSION['id'] = $id;
    }
    
    public static function logout()
    {
        session_destroy();
    }
}

?>
