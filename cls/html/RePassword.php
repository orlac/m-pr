<?php
namespace cls\html;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RePassword
 *
 * @author Antonio
 * orlac@rambler.ru
 */
require_once (dirname(__FILE__).'/Password.php');
class RePassword extends Password {
    //put your code here
    private $pswd;
    
    public function setPasswordField(Password $pswd)
    {
        $this->pswd = $pswd;
    }
    
    public function validate() {
        if(! parent::validate())
            return false;
        if(!$this->pswd)
            throw new Exception ('не задано поле пароля');
        if($this->getDefaultValue() != $this->pswd->getDefaultValue())
        {
            $this->errors = 'повторите пароль';
            return false;
        }
        return true;
    }
    
    public function renderScript()
    {
        $validVar = ($this->form)? $this->form.'.valide' : 'valide';
        ?>
    <script type="text/javascript">
        var <? echo $this->name.'Element'; ?> = {};
        <? echo $this->name.'Element'; ?>.validate = function()
            {
               <?
               if($this->regMatch)
               {
                   ?>
                   jQuery('#field_<? echo $this->name; ?>').keyup(function()
                   {
                       if(this.value != jQuery('#field_<?echo $this->pswd->name?>').attr('value') )
                       {
                           jQuery(this).attr('class', 'badInput');
                           <? echo $validVar ?> = false;
                       } 
                       else
                       {
                           jQuery(this).attr('class', 'goodInput');
                           <? echo $validVar ?> = true;
                       }
                            
                   });
                   <?
               }
               ?>
            }
            jQuery(document).ready(<? echo $this->name.'Element'; ?>.validate());    
    </script>
        <?   
    }
}

?>
