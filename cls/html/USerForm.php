<?php
namespace cls\html;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Form
 *
 * @author Antonio
 * orlac@rambler.ru
 */
require_once (dirname(__FILE__).'/IElement.php');
class USerForm {
    //put your code here
    
    private $elements = array(), $name = null;
    public $request;
    public $errors = array();
    public $showError = false;
    
    /**
     * $request = $_POST/$_GET;
     */
    public function __construct($name = null, &$request = null) {
        $this->name = $name;
        $this->request = ($request !== null)? $request : $_POST;
    }
    
    public function addElement( IElement $element, $key = null )
    {
        $element->setParentFormName($this->name);
        $element->request = &$this->request;
        if($key == null)
            //$this->elements[$element->getKey()] = $element;
            $this->elements[] = $element;
        else
            $this->elements[$key] = $element;
    }
    
    public function getElements( )
    {
        return $this->elements;
    }
    
    protected function checkSend()
    {
        if($this->name)
            if(isset($this->request[$this->name]))
                    return true;
        else
            if(!empty($this->request))
                    return true;
    }
    
    public function validate()
    {
        if(!$this->checkSend())
            return ;
        foreach( $this->elements as $element )
        {
            if( !$element->validate() )
             {
                //print_r($element->name.'<hr>');
                $this->errors[] = $element->errors; 
                //return false;
             }  
        }
        if(count($this->errors) > 0)
        {
            if($this->showError)
            {
                //echo $this->showErrorBox();
            }
            return false;
        }
                
        return true;
    }
    
    public function showErrorBox()
    {
        if(count($this->errors) == 0)
                return '';
        return '<div class="errorBox" style="color: red; border-color: red; border-style: solid; border-width: 1px;padding:10px;">'.implode('<br>', $this->errors).'</div>';
    }
    
    public function data()
    {
        $d = array();
        foreach( $this->elements as $element )
        {
            
            if($this->name != null )
            {
                $d[$element->name] = isset($this->request[$this->name][$element->name]) ? $this->request[$this->name][$element->name] : null;
            }else
            {
                $d[$element->name] = isset($this->request[$element->name]) ? $this->request[$element->name] : null;
            }
            if($element instanceof CheckBox)
            {
                $d[$element->name] = $this->convertChekboxValue($d[$element->name]);
            }
        }
        return $d;
    }
    
    private function convertChekboxValue($value)
    {
        return ($value == 'on') ? 1 : 0;
    }
    
}

?>
