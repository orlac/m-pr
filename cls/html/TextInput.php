<?php
namespace cls\html;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TextInput
 *
 * @author Antonio
 * orlac@rambler.ru
 */
require_once (dirname(__FILE__).'/IElement.php');
class TextInput extends IElement   {
    //put your code here
    
    public function text(){
        return $this->getDefaultValue();
    }
    
    public function label() {
        ?>
        <div class="textFormLabel">
        <?
        parent::label();
        ?>
        </div>
        <?
    }
    
    public function show() 
    {
        $this->label();
        $this->showElementOnly();
    }
    
    public function showElementOnly()
    {
        ?>
<input <?echo ($this->getClass()? " class='".$this->getClass()."' " : "" )?> <?echo $this->htmlAttr;?> type="text" id="field_<?echo $this->name;?>" value="<? echo $this->getDefaultValue(); ?>" name="<?echo $this->getName();?>" />
        <?
    }
    
    public function renderScript()
    {
        $validVar = ($this->form)? $this->form.'.valide' : 'valide';
        ?>
    <script type="text/javascript">
        var <? echo $this->name.'Element'; ?> = {};
        <? echo $this->name.'Element'; ?>.validate = function()
            {
               <?
               if($this->regMatch)
               {
                   ?>
                   jQuery('#field_<? echo $this->name; ?>').keyup(function()
                   {
                       var regPass = <?echo $this->regMatch;?>;
                       if(regPass.test(this.value) != true )
                       {
                            jQuery(this).attr('class', 'badInput');
                            <? echo $validVar ?> = false;
                       }
                       else
                       {
                           jQuery(this).attr('class', 'goodInput');
                           <? echo $validVar ?> = true;
                       }
                            
                            
                   });
                   <?
               }
               ?>
            }
            jQuery(document).ready(<? echo $this->name.'Element'; ?>.validate());    
    </script>
        <?   
    }
    
}

?>
