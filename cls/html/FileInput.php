<?php
namespace cls\html;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of File
 *
 * @author Antonio
 * orlac@rambler.ru
 */
require_once (dirname(__FILE__).'/IElement.php');
class FileInput extends IElement {
    //put your code here
    public $htmlAttr;
    
    const IMAGE = 'image';
    public $type, $path, $fileName;
    
    public function __construct($name, $data = null, &$request = null) {
        parent::__construct($name, $data, $request);
        $this->request = &$_FILES;
    }
    
    public function text(){
        return $this->getDefaultValue();
    }
    
    public function label() {
        ?>
        <div class="textFormLabel">
        <?
        parent::label();
        ?>
        </div>
        <?
    }
    
    public function show() 
    {
        $this->label();
        $this->showElementOnly();
    }
    
    public function showElementOnly()
    {
        ?>
<input <?echo ($this->getClass()? " class='".$this->getClass()."' " : "" )?> <?echo $this->htmlAttr;?> type="file" id="field_<?echo $this->name;?>" value="<? echo $this->getDefaultValue(); ?>" name="<?echo $this->getName();?>" />
        <?
    }
    
    public function validate()
    {
        $this->wasValidate = true;
        $result = $this->getDefaultValue();
        if($result['size'] == 0 && $this->_notNull == true)
        {
            $this->errors = 'загрузите картинку';
            return false;
        }
        if($result['size'] == 0)
        {
            return true;
        }
        //print_r($result);
        if($result['error'] != 0)
        {
            $this->errors = 'oops... some error';
            return false;
        }
        if(!$this->path)
            throw new \Exception ("path is not set");
        $this->fileName = $this->getFileName($result['name']);
        $uploadfile = $this->path.$this->fileName;
        if (!move_uploaded_file($result['tmp_name'], $uploadfile))
        {
            $this->errors = 'oops... failed to save file';
            return false;
        }
        return true;
    }

    protected function getFileName($origin)
    {
        $arr = explode('.', $origin);
        $ext = array_pop($arr);
        return time()."_".md5(implode('.', $arr)).'.'.$ext;
    }
    
    public function getDefaultValue()
    {
        $arr = $this->_getDefaultValue();
        if($arr === null )
            return null;
        $res = array();
        foreach ($arr as $key => $val)
            $res[$key] = $val[$this->name];
        return $res;
    }
    
    protected function _getDefaultValue()
    {
        //print_r($_FILES);
        if($this->form != null)
            return (isset($_FILES[$this->form])) ? $_FILES[$this->form] : null;
        else
            return (isset($_FILES))? $_FILES : null;
    }
}

?>
