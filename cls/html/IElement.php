<?php
namespace cls\html;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Antonio
 */
class IElement {
    //put your code here
    
    protected $_notNull = false;
    protected $data, $_checkValue;
    protected $default, $_label;
    
    public $badClass = 'badInput';
    public $goodClass = 'goodInput';
    
    public $errors;
    
    public $htmlAttr = "style='width: 150px;'", $name, $request;
    
    public $regMatch;
    
    const 
        EMAIL = "/[a-z0-9_-]+(\.[a-z0-9_-]+)*@([0-9a-z][0-9a-z-]*[0-9a-z]\.)+([a-z]{2,4})$/i",
        LOGIN = "/^[a-z0-9_-]{2,16}$/",
        PASS = "/^[a-z0-9_-]{6,18}$/";
    
    public function setMatch($m)
    {
        $this->regMatch = $m;
    }
    
    public function __construct($name, $data = null, &$request = null )
    {
        $this->name = $name;
        $this->data = $data;
        $this->request = ($request !== null)? $request : $_POST;
    }
    
    public function show(){}
    public function showElementOnly(){}
    public function text(){}
    public function showError()
    {
        $style = 'style = "display:none;"';
        if($this->errors && $this->wasValidate)
            $style = 'style = "display:block;"';
        ?>
        <span class="formError" <?echo $style;?> id="error_field_<?echo $this->name;?>"><?echo $this->errors;?></span>
        <?
    }
    
    public function label()
    {
        ?>
        <strong><? echo $this->_label; ?></strong>
        <?
    }
    
    public function setDefault($d)
    {
        $this->default = $d;
        //print_r($this->default."<br>");
    }
    
    public function setLabel($l)
    {
        $this->_label = $l;
    }
    
    public function checkValue($value)
    {
        $this->_checkValue = $value;
    }
    protected $wasValidate = false;
    public function validate()
    {
        $this->wasValidate = true;
        $this->_checkValue = $this->getDefaultValue();
        if(empty($this->_checkValue) && $this->_notNull == true)
        {
            $this->errors = /*$this->name.*/'поле '.$this->_label.'  не должно быть пустым ';
            return false;    
        }
        
        if ( ($this->regMatch !== null) && !preg_match($this->regMatch, $this->getDefaultValue()))
        {
            print_r("1-");
            $this->errors = 'поле '.$this->_label.'  не соответствует формату ';
            return false;
        }
        return true;
    }
    
    protected $form = null;
    public function setParentFormName($form)
    {
        $this->form = $form;
    }
    
    protected function getClass()
    {
        if($this->wasValidate && empty($this->errors))
                return $this->goodClass;
        if($this->wasValidate && $this->errors)
                return $this->badClass;
    }
    
    protected function getSendValue()
    {
        if($this->form != null)
        {
            if(isset($this->request[$this->form][$this->name]))
            {
                return $this->request[$this->form][$this->name];
            }
        }else
        {
            if(isset($this->request[$this->name]))
            {
                return $this->request[$this->name];
            }
        }
        
        return false;
    }
    
    public function getDefaultValue()
    {
        if($this->form != null)
        {
            if(isset($this->request[$this->form]))
            {
                return (isset($this->request[$this->form][$this->name])) ? $this->request[$this->form][$this->name] : null ;
            }else
            {
                return $this->default;
            }
        }else
        {
            if(!empty($this->request))
            {
                return (isset($this->request[$this->name])) ? $this->request[$this->name] : null ;
            }else
            {
                return $this->default;
            }
        }
        return null;
    }
    
    public function getKey()
    {
        return $this->name;
    }
    
    protected function getName()
    {
        if($this->form == null)
            return $this->name;
        else
            return $this->form.'['.$this->name.']';
    }
    
    public function notNUll()
    {
        $this->_notNull = true;
    }
    
//    public function renderScript(){}
}

?>
