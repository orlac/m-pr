<?php
namespace cls\html;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Password
 *
 * @author Antonio
 * orlac@rambler.ru
 */
require_once (dirname(__FILE__).'/TextInput.php');
require_once (dirname(__FILE__).'/IElement.php');
class Password extends TextInput {
    //put your code here
    
    const MATCH_LOGIN = '/^[a-z0-9_-]{1,18}$/';
    //const MATCH_REG = '/^[a-z0-9_-]{6,18}$/';
    
    public function __construct($name, $data = null, &$request = null) {
        parent::__construct($name, $data, $request);
        $this->regMatch = IElement::PASS;
    }
    
    protected function getMatch()
    {
        return ($this->regMatch) ? $this->regMatch : self::MATCH_REG;
    }
    
    public function showElementOnly()
    {
        ?>
<input <?echo ($this->getClass()? " class='".$this->getClass()."' " : "" )?> <?echo $this->htmlAttr;?> type="password" id="field_<?echo $this->name;?>" value="<? echo $this->getDefaultValue(); ?>" name="<?echo $this->getName();?>" />
        <?
    }
    
    public function validate() {
        if(!parent::validate())
            return false;
        if ( ($this->regMatch !== null) && !preg_match($this->regMatch, $this->getDefaultValue()))
        {
            $this->errors = 'пароль должен быть больше 6 символов, состоять из цифр и букв ';
            return false;
        }
        return true;
    }
}

?>
