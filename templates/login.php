<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once (dirname(__FILE__).'/../cls/forms/Login.php');
require_once (dirname(__FILE__).'/../cls/PageNav.php');
require_once (dirname(__FILE__).'/../cls/html/IElement.php');
require_once (dirname(__FILE__).'/../cls/models/USer.php');
if($user = \cls\PageNav::isAuth())
    \cls\PageNav::goToPage (\cls\PageNav::profile);
$form = new \cls\forms\Login('login');
?>
<style>

</style>
<h1>Вход</h1>
<?
$authError = false;
if( $form->validate())
{
    //print_r($form);
    $data = $form->data();
    $user = new cls\models\User();
    $user->email = $data['email'];
    $user->pass = $data['password'];
    $u = $user->getOne();
    //print_r($u);
    if(!$u->id)
    {
        $authError = true;
        $form->errors[] = 'Неверное имя пользователя или пароль';
    }
    else
    {
        \cls\PageNav::auth($u->id);
        \cls\PageNav::goToPage (\cls\PageNav::profile);
    }  
}
?>
<div >
    <form method="POST" id="loginForm" name="login" enctype="multipart/form-data">
        <?
        if($authError)
            echo $form->showErrorBox();
        foreach($form->getElements() as $element)
        {
            ?>
        <div>
            <?
            $element->show();
            $element->showError();
            ?>
        </div>
            <?
        }
        ?>
        <input type="button" id="_submit" value="go" ></input>
    </form>
</div>    
<script type="text/javascript" src="/js/form/form.js"></script>
<script type="text/javascript" src="/js/form/login.js"></script>
