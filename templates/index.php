<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once (dirname(__FILE__).'/../cls/PageNav.php');
?>
<!DOCTYPE html>
<html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="StyleSheet" href="/css/style.css" type="text/css">
        <script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
    </head>
    <body>
        
        <ul class="mainMenu" >
        <li>
            <a href="<?echo \cls\PageNav::getLink(\cls\PageNav::main);?>">Главная</a>
        </li>
        <li>
            <a href="<?echo \cls\PageNav::getLink(\cls\PageNav::registration);?>">Регистрация</a>
        </li>
        <li>
            <a href="<?echo \cls\PageNav::getLink(\cls\PageNav::login);?>">Вход</a>
        </li>
        <li>
            <a href="<?echo \cls\PageNav::getLink(\cls\PageNav::profile);?>">Личный кабинет</a>
        </li>
        <li>
            <a onclick="return (!confirm('Вы уверены'))? false : true; " href="<?echo \cls\PageNav::getLink(\cls\PageNav::logout);?>">Выход</a>
        </li>
        
    </ul>
    <div style="clear: both">    
    <hr>
    <?
    echo $cont;
    ?>
    </div>
    </body>
</html> 

