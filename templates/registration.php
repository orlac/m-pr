<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */ 
require_once (dirname(__FILE__).'/../cls/forms/Registration.php');
require_once (dirname(__FILE__).'/../cls/PageNav.php');
require_once (dirname(__FILE__).'/../cls/html/IElement.php');
require_once (dirname(__FILE__).'/../cls/models/USer.php');
if($user = \cls\PageNav::isAuth())
    \cls\PageNav::goToPage (\cls\PageNav::profile);
$form = new \cls\forms\Registration('registration');
?>
<style>

</style>
<h1>Регистрация</h1>
<?
//print_r($_POST);
//print_r($_FILES);
if( $form->validate())
{
    //print_r($form);
    $data = $form->data();
    $user = new cls\models\User();
    $user->email = $data['email'];
    $user->name = $data['user_name'];
    $u = $user->getOne();
    if($u->id)
        $form->mail->errors = 'такой e-mail уже зарегистрирован';
    else
    {
        $user->pass = $data['password'];
        $user->date_reg = cls\models\User::NOW;
        $user->photo = $form->file->fileName;
        if( $user->save() )
            \cls\PageNav::goToPage (\cls\PageNav::login);
        //print_r($form->data());
    }  
}
//echo $form->showErrorBox();
?>
<div >
<form method="POST" id="regForm" name="registration" enctype="multipart/form-data">
    <?
    foreach($form->getElements() as $element)
    {
        ?>
    <div>
        <?
        $element->show();
        $element->showError();
        ?>
    </div>
        <?
    }
    ?>
    <input type="button" id="_submit" value="go" ></input>
</form>
    </div>
<script type="text/javascript">
    var regPass = <? echo cls\html\IElement::PASS; ?>;
    var regMail = <? echo cls\html\IElement::EMAIL; ?>;
</script>
<script type="text/javascript" src="/js/form/form.js"></script>
<script type="text/javascript" src="/js/form/reg.js"></script>