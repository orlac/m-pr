<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once (dirname(__FILE__).'/../cls/PageNav.php');
require_once (dirname(__FILE__).'/../cls/models/USer.php');

if(!$userId = \cls\PageNav::isAuth())
    \cls\PageNav::goToPage (\cls\PageNav::login);
?>
<h1>Личный кабинет</h1>
<?
$user = new cls\models\User();
$user->id = $userId;
$user = $user->getOne();
if( !$user->id )
    \cls\PageNav::goToPage (\cls\PageNav::login);
?>
<ul>
    <li>
        Имя: <? echo strip_tags( $user->name ); ?>
    </li>
    <li>
        e-mail: <? echo $user->email ?>
    </li>
    <li>
        <?
        if($user->photo)
        {
            ?>
            <img src="<? echo I_URL.$user->photo; ?>"
            <?     
        }
        ?>
    </li>
</ul>
