var validregForm = {};
    validregForm.email = function ()
    {
        if(regMail.test(jQuery('#field_email').attr('value')) != true )
        {
            jQuery('#field_email').attr('class', 'badInput');
            formError.show(jQuery('#error_field_email'), 'Неверный формат e-mail');
            return false;
        }
        else
        {
            jQuery('#field_email').attr('class', 'goodInput');
            formError.hide(jQuery('#error_field_email'));
            return true;
        }
            
    }
    validregForm.password = function ()
    {
        if(regPass.test(jQuery('#field_password').attr('value') ) != true )
        {
            jQuery('#field_password').attr('class', 'badInput');
            formError.show(jQuery('#error_field_password'), 'Пароль должен быть не меньше 6 символов');
            return false;
        }
        else
        {
            jQuery('#field_password').attr('class', 'goodInput');
            formError.hide(jQuery('#error_field_password'));
            return true;
        }
    }
    validregForm.repassword = function ()
    {
        if(jQuery('#field_repassword').attr('value') != jQuery('#field_password').attr('value') )
        {
            jQuery('#field_repassword').attr('class', 'badInput');
            formError.show(jQuery('#error_field_repassword'), 'Пароли не совпвдают');
            return false;
        }   
        else
        {
            jQuery('#field_repassword').attr('class', 'goodInput');
            formError.hide(jQuery('#error_field_repassword'));
            return true;
        }
            
    }
    validregForm.user_name = function ()
    {
        if(jQuery('#field_user_name').attr('value')  == '' )
        {
            jQuery('#field_user_name').attr('class', 'badInput');
            formError.show(jQuery('#error_field_user_name'), 'Пожалуйста, представтесь');
            return false;
        }
        else
        {
            jQuery('#field_user_name').attr('class', 'goodInput');
            formError.hide(jQuery('#error_field_user_name'));
            return true;
        }
    }
  
    
    jQuery(document).ready(function()
    {
       jQuery('#_submit').click(function()
       {
           var errors = 0;
           for(var i in validregForm)
               if(i != 'className')
               {
                   if(validregForm[i]() == false)
                   errors++;
               }
           if(errors == 0)
           {
               jQuery('#regForm').submit();
           }
           //jQuery('#regForm').submit();    
       }); 
       jQuery('#field_email').keyup(validregForm.email); 
       jQuery('#field_password').keyup(validregForm.password); 
       jQuery('#field_repassword').keyup(validregForm.repassword); 
    });    