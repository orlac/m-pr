var formError = {};
  formError.show = function(jElement, text)
  {
      jElement.show();
      jElement.text(text);
  }
  formError.hide = function(jElement)
  {
      jElement.hide();
  }