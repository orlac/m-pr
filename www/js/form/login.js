var validLoginForm = {};
    validLoginForm.email = function ()
    {
        if(jQuery('#field_email').attr('value') == '' )
        {
            jQuery('#field_email').attr('class', 'badInput');
            formError.show(jQuery('#error_field_email'), 'Введите e-mail, указанный при регистрации');
            return false;
        }
        else
        {
            jQuery('#field_email').attr('class', 'goodInput');
            formError.hide(jQuery('#error_field_email'));
            return true;
        }
    }
    validLoginForm.password = function ()
    {
        if(jQuery('#field_password').attr('value')  == '' )
        {
            jQuery('#field_password').attr('class', 'badInput');
            formError.show(jQuery('#error_field_password'), 'Введите пароль, указанный при регистрации');
            return false;
        }
        else
        {
            jQuery('#field_password').attr('class', 'goodInput');
            formError.hide(jQuery('#error_field_password'));
            return true;
        }
    }
    

    
    jQuery(document).ready(function()
    {
       jQuery('#_submit').click(function()
       {
           var errors = 0;
           for(var i in validLoginForm)
               if(i != 'className')
               {
                   if(validLoginForm[i]() == false)
                   errors++;
               }
           if(errors == 0)
           {
               jQuery('#loginForm').submit();
           }
           //jQuery('#loginForm').submit();    
       }); 
    });    