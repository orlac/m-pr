<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();
require_once (dirname(__FILE__).'/../cls/Config.php');
require_once (dirname(__FILE__).'/../cls/PageNav.php');
\cls\Config::connect();

$page = isset($_GET['page']) ? $_GET['page'] : null;

switch($_GET['page'])
{
    case \cls\PageNav::registration:
        $cont = getContent( dirname(__FILE__).'/../templates/registration.php');
        require_once (dirname(__FILE__).'/../templates/index.php');
        break;
    case \cls\PageNav::login:        
        $cont = getContent( dirname(__FILE__).'/../templates/login.php');
        require_once (dirname(__FILE__).'/../templates/index.php');
        break;
    case \cls\PageNav::profile:        
        $cont = getContent( dirname(__FILE__).'/../templates/profile.php');
        require_once (dirname(__FILE__).'/../templates/index.php');
        break;
    case \cls\PageNav::logout:        
        $cont = getContent( dirname(__FILE__).'/../templates/logout.php');
        require_once (dirname(__FILE__).'/../templates/index.php');
        break;
    default :
        require_once (dirname(__FILE__).'/../templates/index.php');
}


function getContent($file)
{
    ob_start();
    require_once ( $file );
    $cont = ob_get_contents();
    ob_end_clean();
    return $cont;
}
?>
